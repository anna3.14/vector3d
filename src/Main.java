//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Vector3D vettore1= new Vector3D(1,0,0);
        Vector3D vettore2= new Vector3D(0,1,0);

        System.out.println(vettore1.getModulo());
        System.out.println(vettore2.getModulo());

        System.out.println(vettore1.moltiplicazioneScalare(vettore1, 3));
        System.out.println(vettore1.prodottoScalare(vettore1, vettore2));
        System.out.println(vettore1.prodottoVettoriale(vettore1, vettore2));
        System.out.println(vettore1.sommaVettori(vettore1, vettore2));
        System.out.println(vettore1.differenzaVettori(vettore1, vettore2));
    }
}