import java.lang.Math;

import static java.lang.Math.sqrt;

public class Vector3D {
    private float dimX;
    private float dimY;
    private float dimZ;
    private float modulo;

    // Costruttore di default
    public Vector3D(){

    }

    // Costruttore con attributi
    public Vector3D(float dimX,float dimY,float dimZ){
        this.dimX=dimX;
        this.dimY=dimY;
        this.dimZ=dimZ;
        float modulo=(float) sqrt((dimX*dimX)+(dimY*dimY)+(dimZ*dimZ));
        this.modulo=modulo;

    }

    // Metodi get
    public float getDimX(){
        return dimX;
    }
    public float getDimY(){
        return dimY;
    }
    public float getDimZ(){
        return dimZ;
    }
    public float getModulo(){
        return modulo;
    }

    // Metodi set
    public void setDimX(float dimX){
        this.dimX=dimX;
    }
    public void setDimY(float dimY){
        this.dimY=dimY;
    }
    public void setDimZ(float dimZ){
        this.dimZ=dimZ;
    }
    public void setModulo(float modulo){
        this.modulo=modulo;
    }

    //Moltiplicazione per uno scalare --> modulo prodotto come risultato

    public float moltiplicazioneScalare(Vector3D vettore, float scalare){
        float risultato=vettore.modulo*scalare;
        return risultato;
    }

    //Somma tra due vettori 3D --> modulo vettore somma come risultato
    public float sommaVettori(Vector3D vettore1, Vector3D vettore2){
        float dimXrisultato=vettore1.dimX+vettore2.dimX;
        float dimYrisultato=vettore1.dimY+vettore2.dimY;
        float dimZrisultato=vettore1.dimZ+vettore2.dimZ;
        float moduloSomma=(float) sqrt((dimXrisultato*dimXrisultato)+(dimYrisultato*dimYrisultato)+(dimZrisultato*dimZrisultato));
        return moduloSomma;
    }

    // Differenza tra due vettori 3d --> modulo vettore differenza come risultato
    public float differenzaVettori(Vector3D vettore1, Vector3D vettore2){
        float dimXrisultato=vettore1.dimX-vettore2.dimX;
        float dimYrisultato=vettore1.dimY-vettore2.dimY;
        float dimZrisultato=vettore1.dimZ-vettore2.dimZ;
        float moduloDifferenza=(float) sqrt((dimXrisultato*dimXrisultato)+(dimYrisultato*dimYrisultato)+(dimZrisultato*dimZrisultato));
        return moduloDifferenza;
    }

    // Prodotto scalare v1*v2=v1x*v2x+v1y*v2y+v1z*v2z
    public float prodottoScalare(Vector3D vettore1, Vector3D vettore2){
        float risultato=(vettore1.dimX*vettore2.dimX)+(vettore1.dimY*vettore2.dimY)+(vettore1.dimZ*vettore2.dimZ);
        return risultato;
    }

    //Prodotto vettoriale --> risultato
    public float prodottoVettoriale(Vector3D vettore1, Vector3D vettore2){
        float dimXrisultato=((vettore1.dimY*vettore2.dimZ)-(vettore1.dimZ*vettore2.dimY));
        float dimYrisultato=((vettore1.dimZ*vettore2.dimX)-(vettore1.dimX*vettore2.dimZ));
        float dimZrisultato=((vettore1.dimX*vettore2.dimY)-(vettore1.dimY*vettore2.dimX));
        float moduloProdotto=(float) sqrt((dimXrisultato*dimXrisultato)+(dimYrisultato*dimYrisultato)+(dimZrisultato*dimZrisultato));
        return moduloProdotto;
    }







}
